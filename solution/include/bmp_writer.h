#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE *out, struct image const* img );

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H
