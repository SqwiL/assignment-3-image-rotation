#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H

#define BMP_TYPE 19778
#define RESERVED 0
#define BMP_OFF_BITS 54
#define BMP_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define PELS_PER_METER 0
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

#include "image.h"
#include <stdio.h>

struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H
