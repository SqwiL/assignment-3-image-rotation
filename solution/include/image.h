#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) pixel{
    uint8_t b, g, r;
};

struct  image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(const uint64_t width, const uint64_t height);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
