#include "../include/bmp_header.h"
#include "../include/bmp_reader.h"

static uint8_t padding(const uint64_t width) {
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    *img = create_image(header.biWidth, header.biHeight);


    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
            free(img);
            return READ_ERROR;
        }
        if (fseek(in, padding(img->width), SEEK_CUR)) {
            free(img);
            return READ_ERROR;
        }
    }

    return READ_OK;
}
