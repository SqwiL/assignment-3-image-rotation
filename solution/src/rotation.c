#include "../include/rotation.h"
#include "../include/image.h"

struct image rotate( struct image const source) {
    struct image new_image = create_image(source.height, source.width);
    for (size_t i = 0; i < new_image.height; i++) {
        for (size_t j = 0; j < new_image.width; j++) {
            new_image.data[new_image.width * i + j] = source.data[(source.height - j - 1) * source.width + i];
        }
    }
    return new_image;
}
