#include "../include/bmp_header.h"
#include "../include/bmp_writer.h"

static uint8_t padding(const uint64_t width) {
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel)* img->height * img->width + padding(img->width)),
            .bfReserved = RESERVED,
            .bOffBits = BMP_OFF_BITS,
            .biSize = BMP_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = (sizeof(struct pixel) * img->height * img->width + padding(img->width)),
            .biXPelsPerMeter = PELS_PER_METER,
            .biYPelsPerMeter = PELS_PER_METER,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT
};

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width )
        {
            return WRITE_ERROR;
        }
        if (fseek(out, padding(img->width), SEEK_CUR))
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;


}
