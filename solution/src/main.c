#include "../include/bmp_header.h"
#include "../include/bmp_reader.h"
#include "../include/bmp_writer.h"
#include "../include/rotation.h"

int main( int argc,char *argv[] ) {

    if (argc != 3){
        fprintf(stderr, "Incorrect number of arguments\n");
        return -1;
    }

    FILE* input_file;

    input_file = fopen(argv[1], "r");

    if (input_file == NULL) {
        fprintf(stderr, "Error opening file\n");
        return -1;
    }

    struct image image = {0};

    from_bmp(input_file, &image);

    fclose(input_file);

    struct image new_image = rotate(image);

    FILE* output_file;

    output_file = fopen(argv[2], "w");

    if (output_file == NULL) {
        fprintf(stderr, "Error opening file\n");
        return -1;
    }

    to_bmp(output_file, &new_image);

    fclose(output_file);
    return 0;
}
